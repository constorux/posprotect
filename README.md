# PosProtect Demo
A demo for the *posProtect* extension for single user *Location Privacy Protection Mechanisms* introduced in the Seminar Paper by Robin Naumann at the University of Ulm

The app allows the selection of a location that aims to protect the user's privacy while not reducing the 'quality' of the location to greatly. 
This is done by analyzing the user's location over time.

The exact functionality is described in the seminar paper [I will link this paper once it passes]

## Screenshots
<img width="160px" class="screenshot" src="./screenshots/main_0.jpg">
<img width="160px" class="screenshot" src="./screenshots/main_1.jpg">
<img width="160px" class="screenshot" src="./screenshots/prefs_1.jpg">

## Installing
the app is currently available only for Android (as running the neccessary background service on iOS is not as simple due to rate limitations)

To install the release simply install the `.apk` file from the `release` directory.

## building
The app was written in Flutter. To run the project make sure you have a recent version of Flutter & Dart installed. Then run (from the project root directory):
1. `flutter pub get`
2. `flutter run`


>### Disclaimer
>This project was created by a student at uulm and is not >officially endorced by, or affiliated with the unversity.
