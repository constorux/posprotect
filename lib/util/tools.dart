import 'dart:math';

import 'package:flutter/material.dart';
import 'package:latlong2/latlong.dart';

class Tools {
  static showSnackbar(BuildContext context, String message) {
    ScaffoldMessenger.of(context)
      ..hideCurrentSnackBar()
      ..showSnackBar(
        SnackBar(
          content: Text(message),
        ),
      );
  }

  static double euclideanDistance(LatLng p1, LatLng p2) =>
      sqrt(pow(p1.latitude - p2.latitude, 2) +
          pow(p1.longitude - p2.longitude, 2)) +
      0.00001;
}
