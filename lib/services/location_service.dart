import 'package:flutter/services.dart';
import 'package:location/location.dart';
import 'package:sem_posprotect/services/local_data_service.dart';

class LocationService {
  static Future<bool> checkEnabled() async {
    Location location = Location();

    bool _serviceEnabled;
    PermissionStatus _permissionGranted;

    _serviceEnabled = await location.serviceEnabled();
    if (!_serviceEnabled) {
      _serviceEnabled = await location.requestService();
      if (!_serviceEnabled) {
        return false;
      }
    }

    _permissionGranted = await location.hasPermission();
    if (_permissionGranted == PermissionStatus.denied) {
      _permissionGranted = await location.requestPermission();
    }

    return _permissionGranted == PermissionStatus.granted;
  }

  static Future<PosTrace> getLocation() async {
    //if (!(await checkEnabled())) return null;

    Location location = Location();

    LocationData? _loc;
    try {
      _loc = await location.getLocation();
    } on PlatformException {
      throw Exception("could not get location");
    }

    if (_loc.latitude == null || _loc.longitude == null) {
      throw Exception("location fields were null");
    }

    return PosTrace(
        latitude: _loc.latitude!,
        longitude: _loc.longitude!,
        timestamp: DateTime.now().millisecondsSinceEpoch);
  }
}
