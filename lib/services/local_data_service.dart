import 'dart:async';

import 'package:latlong2/latlong.dart';
import 'package:shared_preferences/shared_preferences.dart';

const lsKeyTraces = "traces";

class LocalDataService {
  static SharedPreferences? _prefs;
  static FutureOr<SharedPreferences> _getSPInstance() async {
    _prefs ??= await SharedPreferences.getInstance();
    _prefs!.reload();
    return _prefs!;
  }

  static Future<List<PosTrace>> getTraces() async {
    return ((await _getSPInstance()).getStringList(lsKeyTraces)!)
        .map((e) => PosTrace.decode(e))
        .toList();
    //await prefs.setInt('counter', counter);
  }

  static Future<void> deleteAllTraces() async {
    (await _getSPInstance()).setStringList(lsKeyTraces, []);
  }

  static addTrace(PosTrace trace) async {
    List<String> lst =
        (await _getSPInstance()).getStringList(lsKeyTraces) ?? [];
    var result = lst.reversed.take(499).toList().reversed.toList();
    result.add(trace.encode());
    (await _getSPInstance()).setStringList(lsKeyTraces, result);
  }
}

class PosTrace {
  final double latitude;
  final double longitude;
  final int timestamp;

  LatLng get position => LatLng(latitude, longitude);

  PosTrace(
      {required this.latitude,
      required this.longitude,
      required this.timestamp});

  factory PosTrace.decode(String s) {
    try {
      List<String> parts = s.split(":");
      return PosTrace(
          latitude: double.parse(parts[0]),
          longitude: double.parse(parts[1]),
          timestamp: int.parse(parts[2]));
    } catch (e) {
      throw Exception("could not parse PosTrace");
    }
  }

  String encode() {
    return "$latitude:$longitude:$timestamp";
  }
}
