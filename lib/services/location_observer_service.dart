import 'package:background_locator/background_locator.dart';
import 'package:background_locator/location_dto.dart';
import 'package:background_locator/settings/android_settings.dart';
import 'package:background_locator/settings/locator_settings.dart';
import 'package:flutter/material.dart';
import 'package:sem_posprotect/services/local_data_service.dart';

class LocationObserverService {
  static void start() async {
    WidgetsFlutterBinding.ensureInitialized();
    await BackgroundLocator.initialize();
    BackgroundLocator.registerLocationUpdate(LocationCallbackHandler.callback,
        autoStop: false,
        androidSettings: const AndroidSettings(
            accuracy: LocationAccuracy.NAVIGATION,
            interval: 10,
            distanceFilter: 0,
            client: LocationClient.google,
            androidNotificationSettings: AndroidNotificationSettings(
              notificationChannelName: 'Location tracking',
              notificationTitle: 'PosProtect Observer',
              notificationMsg: 'geo-position tracking is active',
              notificationBigMsg:
                  'the app regularly tracks your geo-position to protect sensitive areas in your environment',
              notificationIconColor: Colors.grey,
            )));
  }
}

class LocationCallbackHandler {
  static Future<void> callback(LocationDto locationDto) async {
    LocalDataService.addTrace(PosTrace(
        latitude: locationDto.latitude,
        longitude: locationDto.longitude,
        timestamp: DateTime.now().millisecondsSinceEpoch));
    print("saved location trace");
  }

  static Future<void> notificationCallback() async {
    print('***notificationCallback');
  }
}
