import 'dart:math';

import 'package:flutter/material.dart';
import 'package:latlong2/latlong.dart';
import 'package:sem_posprotect/util/tools.dart';

import 'local_data_service.dart';

class AnalysisPoint {
  final LatLng anchor;
  final double score;

  AnalysisPoint({required this.anchor, this.score = 10});

  AnalysisPoint copyWith({required double score}) {
    return AnalysisPoint(anchor: anchor, score: score);
  }
}

class AnalyserOptions {
  final double gridSize;
  final double distPenalty;
  final double timePenalty;
  final double padding;
  final double sensitiveAreaPenalty;

  AnalyserOptions(
      {this.gridSize = 0.0015,
      this.distPenalty = 2.7,
      this.timePenalty = 3,
      this.padding = 10,
      this.sensitiveAreaPenalty = 3});

  AnalyserOptions copyWith(
      {double? gridSize,
      double? distPenalty,
      double? timePenalty,
      double? padding,
      double? sensitiveAreaPenalty}) {
    return AnalyserOptions(
        gridSize: gridSize ?? this.gridSize,
        distPenalty: distPenalty ?? this.distPenalty,
        timePenalty: timePenalty ?? this.timePenalty,
        padding: padding ?? this.padding,
        sensitiveAreaPenalty:
            sensitiveAreaPenalty ?? this.sensitiveAreaPenalty);
  }
}

class AnalyserResult {
  List<AnalysisPoint> areas;
  List<LatLng> sensitiveAreas;
  LatLng? selectedLocation;
  double gridSize;

  AnalyserResult(
      {this.areas = const [],
      this.sensitiveAreas = const [],
      this.selectedLocation,
      this.gridSize = 0.0015});
}

class AnalyserService {
  static AnalyserResult analyzeTraces(
      List<PosTrace> traces, AnalyserOptions options) {
    // performance improvement
    traces = traces.reversed.take(500).toList().reversed.toList();
    AnalyserResult result = AnalyserResult(gridSize: options.gridSize);
    result.areas = _generateGrid(traces, options.gridSize, options.padding);
    result.areas = _generateHeatmap(
        traces, result.areas, options.distPenalty, options.timePenalty);
    result = _obscureSensitiveAreas(
        traces, result, options.sensitiveAreaPenalty, 0.95);
    result.areas = _normalizeScores(result.areas);
    result = _getSelectedLocation(result);

    return result;
  }

  static List<AnalysisPoint> _generateGrid(
      List<PosTrace> traces, double gridSize, padding) {
    print("analyzing traces: generating grid");
    List<AnalysisPoint> result = [];
    double minLong = double.infinity,
        minLat = double.infinity,
        maxLong = double.negativeInfinity,
        maxLat = double.negativeInfinity;

    for (var trace in traces) {
      if (minLat > trace.latitude) minLat = trace.latitude;
      if (maxLat < trace.latitude) maxLat = trace.latitude;
      if (minLong > trace.longitude) minLong = trace.longitude;
      if (maxLong < trace.longitude) maxLong = trace.longitude;
    }

    minLong -= gridSize * padding;
    minLat -= gridSize * padding;
    maxLong += gridSize * padding;
    maxLat += gridSize * padding;

    for (int y = 0; minLong + (y * gridSize) < maxLong; y++) {
      for (int x = 0; minLat + (x * gridSize) < maxLat; x++) {
        result.add(AnalysisPoint(
            anchor: LatLng(minLat + (x * gridSize), minLong + (y * gridSize))));
      }
    }
    return result;
  }

  static double _calcIndividualScore(
      LatLng pos, PosTrace trace, double distPenalty, double timePenalty) {
    const timeFactor = 1 / (1000 * 60 * 5);
    const distFactor = 1;

    // calculate euclidean distance between points
    double d = Tools.euclideanDistance(pos, trace.position) * 1000;

    // calculate time difference to trace
    int tDiff = DateTime.now().millisecondsSinceEpoch - trace.timestamp;

    return min(
        1,
        (1 /
            (pow(d * distFactor, distPenalty) +
                pow(tDiff * timeFactor, timePenalty))));
  }

  static double _generateScore(LatLng pos, List<PosTrace> traces,
      double distPenalty, double timePenalty) {
    double bestScore = 0;
    for (var trace in traces) {
      bestScore = max(bestScore,
          _calcIndividualScore(pos, trace, distPenalty, timePenalty));
    }
    return bestScore;
  }

  static List<AnalysisPoint> _normalizeScores(List<AnalysisPoint> squares) {
    print("analyzing traces: normalizing scores");
    double factor = 1 / squares.fold(0.0, (p, e) => max(p, e.score));
    return squares.map((e) => e.copyWith(score: e.score * factor)).toList();
  }

  static List<AnalysisPoint> _generateHeatmap(List<PosTrace> traces,
      List<AnalysisPoint> squares, double distPenalty, double timePenalty) {
    print("analyzing traces: generating heatmap");
    List<AnalysisPoint> result = [];
    for (var square in squares) {
      result.add(square.copyWith(
          score:
              _generateScore(square.anchor, traces, distPenalty, timePenalty)));
    }

    return result;
  }

  static List<LatLng> identifySensitiveAreas(List<PosTrace> traces,
      {double distThreshold = 0.002, double timeThreshold = 5 * 60 * 1000}) {
    List<LatLng> result = [];
    for (var trace in traces) {
      var areas = traces
          .where((e) =>
              Tools.euclideanDistance(e.position, trace.position) <
                  distThreshold &&
              (e.timestamp - trace.timestamp).abs() < timeThreshold)
          .length;
      if (areas > 10) result.add(trace.position);
    }
    for (int i = 0; i < result.length; i++) {
      result.removeWhere((t) =>
          Tools.euclideanDistance(result[i], t) < 0.001 && t != result[i]);
    }
    return result;
  }

  static AnalyserResult _getSelectedLocation(AnalyserResult result) {
    if (result.areas.isEmpty) return result;

    var highestScore = result.areas.fold<AnalysisPoint>(
        result.areas.first, (r, e) => e.score > r.score ? e : r);

    result.selectedLocation = LatLng(highestScore.anchor.latitude,
        highestScore.anchor.longitude + result.gridSize / 3);

    return result;
  }

  static AnalyserResult _obscureSensitiveAreas(List<PosTrace> traces,
      AnalyserResult result, double distPenalty, double factor) {
    print("analyzing traces: obscuring sensitive areas");
    List<AnalysisPoint> res = [];
    List<LatLng> sensitiveAreas = identifySensitiveAreas(traces);
    for (var square in result.areas) {
      var maxPenalty = 0.0;
      for (var sa in sensitiveAreas) {
        double d = Tools.euclideanDistance(sa, square.anchor) * 1000;
        //if (d > 10) continue;
        double pen = min(1, (1 / (pow(d, distPenalty))));

        maxPenalty = max(maxPenalty, pen).clamp(0, 1);
      }
      res.add(square.copyWith(score: square.score * (1 - maxPenalty * factor)));
    }
    return AnalyserResult(areas: res, sensitiveAreas: sensitiveAreas);
  }
}
