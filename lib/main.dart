import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sem_posprotect/services/location_observer_service.dart';
import 'package:sem_posprotect/services/location_service.dart';
import 'package:sem_posprotect/widgets/home_page.dart';

import 'cubits/analysis_cubit/analysis_cubit.dart';
import 'cubits/analysis_options_cubit/analysisoptions_cubit.dart';
import 'cubits/map_cubit/map_cubit.dart';
import 'cubits/map_options_cubit/map_options_cubit.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  Widget globalProviders({required Widget child}) {
    LocationService.checkEnabled();
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) => MapCubit(),
        ),
        BlocProvider<AnalysisCubit>(
          create: (c) => AnalysisCubit(),
        ),
        BlocProvider<AnalysisoptionsCubit>(
          create: (c) => AnalysisoptionsCubit(),
        ),
        BlocProvider<MapOptionsCubit>(
          create: (c) => MapOptionsCubit(),
        )
      ],
      child: child,
    );
  }

  MyApp({Key? key}) : super(key: key) {
    LocationObserverService.start();
  }

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return globalProviders(
        child: MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        appBarTheme: const AppBarTheme(
            foregroundColor: Colors.black,
            centerTitle: true,
            backgroundColor: Colors.transparent,
            elevation: 0),
        canvasColor: Colors.white,
        primarySwatch: _createMaterialColor(122, 153, 172),
      ),
      home: const HomePage(),
    ));
  }

  //swatch creation
  static Color _colorWithB(int r, int g, int b, num m) {
    return Color.fromARGB(
        255, (r * m).round(), (g * m).round(), (b * m).round());
  }

  static _createMaterialColor(int r, int g, int b) {
    return MaterialColor(Color.fromARGB(255, r, g, b).value, {
      50: _colorWithB(r, g, b, 2),
      100: _colorWithB(r, g, b, 1.8),
      200: _colorWithB(r, g, b, 1.6),
      300: _colorWithB(r, g, b, 1.4),
      350: _colorWithB(r, g, b, 1.3),
      400: _colorWithB(r, g, b, 1.2),
      500: _colorWithB(r, g, b, 1),
      600: _colorWithB(r, g, b, 0.8),
      700: _colorWithB(r, g, b, 0.6),
      800: _colorWithB(r, g, b, 0.4),
      900: _colorWithB(r, g, b, 0.2),
    });
  }
}
