import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:sem_posprotect/services/analyser_service.dart';

part 'analysisoptions_cubit.freezed.dart';
part 'analysisoptions_state.dart';

class AnalysisoptionsCubit extends Cubit<AnalysisoptionsState> {
  AnalysisoptionsCubit()
      : super(AnalysisoptionsState.neutral(AnalyserOptions()));

  changeOption(AnalyserOptions options) {
    emit(AnalysisoptionsState.neutral(options));
  }
}
