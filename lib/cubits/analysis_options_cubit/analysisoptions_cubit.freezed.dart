// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'analysisoptions_cubit.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$AnalysisoptionsStateTearOff {
  const _$AnalysisoptionsStateTearOff();

  _Neutral neutral(AnalyserOptions options) {
    return _Neutral(
      options,
    );
  }
}

/// @nodoc
const $AnalysisoptionsState = _$AnalysisoptionsStateTearOff();

/// @nodoc
mixin _$AnalysisoptionsState {
  AnalyserOptions get options => throw _privateConstructorUsedError;

  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(AnalyserOptions options) neutral,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(AnalyserOptions options)? neutral,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(AnalyserOptions options)? neutral,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Neutral value) neutral,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Neutral value)? neutral,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Neutral value)? neutral,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $AnalysisoptionsStateCopyWith<AnalysisoptionsState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AnalysisoptionsStateCopyWith<$Res> {
  factory $AnalysisoptionsStateCopyWith(AnalysisoptionsState value,
          $Res Function(AnalysisoptionsState) then) =
      _$AnalysisoptionsStateCopyWithImpl<$Res>;
  $Res call({AnalyserOptions options});
}

/// @nodoc
class _$AnalysisoptionsStateCopyWithImpl<$Res>
    implements $AnalysisoptionsStateCopyWith<$Res> {
  _$AnalysisoptionsStateCopyWithImpl(this._value, this._then);

  final AnalysisoptionsState _value;
  // ignore: unused_field
  final $Res Function(AnalysisoptionsState) _then;

  @override
  $Res call({
    Object? options = freezed,
  }) {
    return _then(_value.copyWith(
      options: options == freezed
          ? _value.options
          : options // ignore: cast_nullable_to_non_nullable
              as AnalyserOptions,
    ));
  }
}

/// @nodoc
abstract class _$NeutralCopyWith<$Res>
    implements $AnalysisoptionsStateCopyWith<$Res> {
  factory _$NeutralCopyWith(_Neutral value, $Res Function(_Neutral) then) =
      __$NeutralCopyWithImpl<$Res>;
  @override
  $Res call({AnalyserOptions options});
}

/// @nodoc
class __$NeutralCopyWithImpl<$Res>
    extends _$AnalysisoptionsStateCopyWithImpl<$Res>
    implements _$NeutralCopyWith<$Res> {
  __$NeutralCopyWithImpl(_Neutral _value, $Res Function(_Neutral) _then)
      : super(_value, (v) => _then(v as _Neutral));

  @override
  _Neutral get _value => super._value as _Neutral;

  @override
  $Res call({
    Object? options = freezed,
  }) {
    return _then(_Neutral(
      options == freezed
          ? _value.options
          : options // ignore: cast_nullable_to_non_nullable
              as AnalyserOptions,
    ));
  }
}

/// @nodoc

class _$_Neutral implements _Neutral {
  const _$_Neutral(this.options);

  @override
  final AnalyserOptions options;

  @override
  String toString() {
    return 'AnalysisoptionsState.neutral(options: $options)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _Neutral &&
            (identical(other.options, options) || other.options == options));
  }

  @override
  int get hashCode => Object.hash(runtimeType, options);

  @JsonKey(ignore: true)
  @override
  _$NeutralCopyWith<_Neutral> get copyWith =>
      __$NeutralCopyWithImpl<_Neutral>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(AnalyserOptions options) neutral,
  }) {
    return neutral(options);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(AnalyserOptions options)? neutral,
  }) {
    return neutral?.call(options);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(AnalyserOptions options)? neutral,
    required TResult orElse(),
  }) {
    if (neutral != null) {
      return neutral(options);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Neutral value) neutral,
  }) {
    return neutral(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Neutral value)? neutral,
  }) {
    return neutral?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Neutral value)? neutral,
    required TResult orElse(),
  }) {
    if (neutral != null) {
      return neutral(this);
    }
    return orElse();
  }
}

abstract class _Neutral implements AnalysisoptionsState {
  const factory _Neutral(AnalyserOptions options) = _$_Neutral;

  @override
  AnalyserOptions get options;
  @override
  @JsonKey(ignore: true)
  _$NeutralCopyWith<_Neutral> get copyWith =>
      throw _privateConstructorUsedError;
}
