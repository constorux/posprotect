part of 'analysisoptions_cubit.dart';

@freezed
class AnalysisoptionsState with _$AnalysisoptionsState {
  const factory AnalysisoptionsState.neutral(AnalyserOptions options) = _Neutral;
}
