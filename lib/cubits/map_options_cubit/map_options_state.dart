part of 'map_options_cubit.dart';

@freezed
class MapOptionsState with _$MapOptionsState {
  const factory MapOptionsState.initial(bool showTraces, bool showAreas,
      bool showSensitiveAreas, bool showPosition) = _Initial;
}
