// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'map_options_cubit.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$MapOptionsStateTearOff {
  const _$MapOptionsStateTearOff();

  _Initial initial(bool showTraces, bool showAreas, bool showSensitiveAreas,
      bool showPosition) {
    return _Initial(
      showTraces,
      showAreas,
      showSensitiveAreas,
      showPosition,
    );
  }
}

/// @nodoc
const $MapOptionsState = _$MapOptionsStateTearOff();

/// @nodoc
mixin _$MapOptionsState {
  bool get showTraces => throw _privateConstructorUsedError;
  bool get showAreas => throw _privateConstructorUsedError;
  bool get showSensitiveAreas => throw _privateConstructorUsedError;
  bool get showPosition => throw _privateConstructorUsedError;

  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(bool showTraces, bool showAreas,
            bool showSensitiveAreas, bool showPosition)
        initial,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(bool showTraces, bool showAreas, bool showSensitiveAreas,
            bool showPosition)?
        initial,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(bool showTraces, bool showAreas, bool showSensitiveAreas,
            bool showPosition)?
        initial,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $MapOptionsStateCopyWith<MapOptionsState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $MapOptionsStateCopyWith<$Res> {
  factory $MapOptionsStateCopyWith(
          MapOptionsState value, $Res Function(MapOptionsState) then) =
      _$MapOptionsStateCopyWithImpl<$Res>;
  $Res call(
      {bool showTraces,
      bool showAreas,
      bool showSensitiveAreas,
      bool showPosition});
}

/// @nodoc
class _$MapOptionsStateCopyWithImpl<$Res>
    implements $MapOptionsStateCopyWith<$Res> {
  _$MapOptionsStateCopyWithImpl(this._value, this._then);

  final MapOptionsState _value;
  // ignore: unused_field
  final $Res Function(MapOptionsState) _then;

  @override
  $Res call({
    Object? showTraces = freezed,
    Object? showAreas = freezed,
    Object? showSensitiveAreas = freezed,
    Object? showPosition = freezed,
  }) {
    return _then(_value.copyWith(
      showTraces: showTraces == freezed
          ? _value.showTraces
          : showTraces // ignore: cast_nullable_to_non_nullable
              as bool,
      showAreas: showAreas == freezed
          ? _value.showAreas
          : showAreas // ignore: cast_nullable_to_non_nullable
              as bool,
      showSensitiveAreas: showSensitiveAreas == freezed
          ? _value.showSensitiveAreas
          : showSensitiveAreas // ignore: cast_nullable_to_non_nullable
              as bool,
      showPosition: showPosition == freezed
          ? _value.showPosition
          : showPosition // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc
abstract class _$InitialCopyWith<$Res>
    implements $MapOptionsStateCopyWith<$Res> {
  factory _$InitialCopyWith(_Initial value, $Res Function(_Initial) then) =
      __$InitialCopyWithImpl<$Res>;
  @override
  $Res call(
      {bool showTraces,
      bool showAreas,
      bool showSensitiveAreas,
      bool showPosition});
}

/// @nodoc
class __$InitialCopyWithImpl<$Res> extends _$MapOptionsStateCopyWithImpl<$Res>
    implements _$InitialCopyWith<$Res> {
  __$InitialCopyWithImpl(_Initial _value, $Res Function(_Initial) _then)
      : super(_value, (v) => _then(v as _Initial));

  @override
  _Initial get _value => super._value as _Initial;

  @override
  $Res call({
    Object? showTraces = freezed,
    Object? showAreas = freezed,
    Object? showSensitiveAreas = freezed,
    Object? showPosition = freezed,
  }) {
    return _then(_Initial(
      showTraces == freezed
          ? _value.showTraces
          : showTraces // ignore: cast_nullable_to_non_nullable
              as bool,
      showAreas == freezed
          ? _value.showAreas
          : showAreas // ignore: cast_nullable_to_non_nullable
              as bool,
      showSensitiveAreas == freezed
          ? _value.showSensitiveAreas
          : showSensitiveAreas // ignore: cast_nullable_to_non_nullable
              as bool,
      showPosition == freezed
          ? _value.showPosition
          : showPosition // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$_Initial implements _Initial {
  const _$_Initial(this.showTraces, this.showAreas, this.showSensitiveAreas,
      this.showPosition);

  @override
  final bool showTraces;
  @override
  final bool showAreas;
  @override
  final bool showSensitiveAreas;
  @override
  final bool showPosition;

  @override
  String toString() {
    return 'MapOptionsState.initial(showTraces: $showTraces, showAreas: $showAreas, showSensitiveAreas: $showSensitiveAreas, showPosition: $showPosition)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _Initial &&
            (identical(other.showTraces, showTraces) ||
                other.showTraces == showTraces) &&
            (identical(other.showAreas, showAreas) ||
                other.showAreas == showAreas) &&
            (identical(other.showSensitiveAreas, showSensitiveAreas) ||
                other.showSensitiveAreas == showSensitiveAreas) &&
            (identical(other.showPosition, showPosition) ||
                other.showPosition == showPosition));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, showTraces, showAreas, showSensitiveAreas, showPosition);

  @JsonKey(ignore: true)
  @override
  _$InitialCopyWith<_Initial> get copyWith =>
      __$InitialCopyWithImpl<_Initial>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(bool showTraces, bool showAreas,
            bool showSensitiveAreas, bool showPosition)
        initial,
  }) {
    return initial(showTraces, showAreas, showSensitiveAreas, showPosition);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(bool showTraces, bool showAreas, bool showSensitiveAreas,
            bool showPosition)?
        initial,
  }) {
    return initial?.call(
        showTraces, showAreas, showSensitiveAreas, showPosition);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(bool showTraces, bool showAreas, bool showSensitiveAreas,
            bool showPosition)?
        initial,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(showTraces, showAreas, showSensitiveAreas, showPosition);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
  }) {
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
  }) {
    return initial?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class _Initial implements MapOptionsState {
  const factory _Initial(bool showTraces, bool showAreas,
      bool showSensitiveAreas, bool showPosition) = _$_Initial;

  @override
  bool get showTraces;
  @override
  bool get showAreas;
  @override
  bool get showSensitiveAreas;
  @override
  bool get showPosition;
  @override
  @JsonKey(ignore: true)
  _$InitialCopyWith<_Initial> get copyWith =>
      throw _privateConstructorUsedError;
}
