import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'map_options_state.dart';
part 'map_options_cubit.freezed.dart';

class MapOptionsCubit extends Cubit<MapOptionsState> {
  MapOptionsCubit()
      : super(const MapOptionsState.initial(true, false, false, false));

  setOption(MapOptionsState state) => emit(state);
}
