import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:sem_posprotect/services/analyser_service.dart';
import 'package:sem_posprotect/services/local_data_service.dart';

part 'map_state.dart';
part 'map_cubit.freezed.dart';

class MapCubit extends Cubit<MapState> {
  MapCubit() : super(const MapState.loading()) {
    refresh();
  }

  Future<int> refresh(
      {double gridSize = 0.0015,
      double timePenalty = 3,
      double distPenalty = 2.7,
      double sensitiveAreaPenalty = 3}) async {
    emit(const MapState.loading());
    try {
      var traces = await LocalDataService.getTraces();
      emit(MapState.neutral(traces));
      return traces.length;
    } catch (e) {
      emit(MapState.error("$e"));
      return -1;
    }
  }

  deleteTraces() async {
    await LocalDataService.deleteAllTraces();
    await refresh();
  }
}
