part of 'map_cubit.dart';

@freezed
class MapState with _$MapState {
  const factory MapState.error(String msg) = _Error;
  const factory MapState.loading() = _Loading;
  //const factory MapState.traces(List<PosTrace> traces) = _Traces;
  //const factory MapState.analyzing() = _Analyzing;
  const factory MapState.neutral(
      List<PosTrace> traces) = _Neutral;
}
