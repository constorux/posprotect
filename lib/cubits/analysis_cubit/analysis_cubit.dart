import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:sem_posprotect/services/analyser_service.dart';
import 'package:sem_posprotect/services/local_data_service.dart';

part 'analysis_state.dart';
part 'analysis_cubit.freezed.dart';

class AnalysisCubit extends Cubit<AnalysisState> {
  AnalysisCubit() : super(const AnalysisState.initial());

  reset() {
    emit(const AnalysisState.initial());
  }

  analyse(List<PosTrace> traces, AnalyserOptions options) async {
    try {
      emit(const AnalysisState.analyzing());
      var result = AnalyserService.analyzeTraces(traces, options);
      emit(AnalysisState.completed(result));
    } catch (e) {
      emit(AnalysisState.error("$e"));
    }
  }
}
