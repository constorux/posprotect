part of 'analysis_cubit.dart';

@freezed
class AnalysisState with _$AnalysisState {
  const factory AnalysisState.initial() = _Initial;
  const factory AnalysisState.analyzing() = _Analyzing;
  const factory AnalysisState.error(String msg) = _Error;
  const factory AnalysisState.completed(AnalyserResult result) = _Completed;
}
