import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sem_posprotect/cubits/map_cubit/map_cubit.dart';
import 'package:sem_posprotect/services/local_data_service.dart';

class TracesPage extends StatelessWidget {
  const TracesPage({Key? key}) : super(key: key);

  static void navigate(BuildContext context) {
    Navigator.push(
        context,
        MaterialPageRoute<void>(
            builder: (_) => const TracesPage(),
            settings: const RouteSettings(name: "TracesPage")));
  }

  Widget _elementRow({required String label, required String value}) {
    return Row(
      children: [
        Container(
          width: 100,
          child: Text("$label:"),
        ),
        Expanded(
          child: Text(
            value,
            style: const TextStyle(fontWeight: FontWeight.bold),
          ),
        )
      ],
    );
  }

  Widget _traceElement(PosTrace trace) {
    return Container(
      margin: EdgeInsets.only(top: 10),
      padding: EdgeInsets.all(10),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5), color: Colors.grey.shade200),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          _elementRow(label: "latitude", value: trace.latitude.toString()),
          _elementRow(label: "longitude", value: trace.longitude.toString()),
          _elementRow(label: "timestamp", value: trace.timestamp.toString())
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("recorded traces"),
          centerTitle: true,
        ),
        body: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10),
          child: BlocBuilder<MapCubit, MapState>(
              builder: (context, state) => state.when(
                  error: (msg) => Center(child: Text(msg)),
                  loading: () =>
                      const Center(child: CircularProgressIndicator()),
                  neutral: (traces) => ListView.builder(
                      itemCount: traces.length,
                      itemBuilder: (context, i) => _traceElement(traces[i])))),
        ));
  }
}
