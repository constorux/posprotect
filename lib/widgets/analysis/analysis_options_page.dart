import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sem_posprotect/cubits/analysis_cubit/analysis_cubit.dart';
import 'package:sem_posprotect/cubits/analysis_options_cubit/analysisoptions_cubit.dart';

class AnalysisOptionsPage extends StatelessWidget {
  const AnalysisOptionsPage({Key? key}) : super(key: key);

  static void navigate(BuildContext context) {
    Navigator.push(
        context,
        MaterialPageRoute<void>(
            builder: (_) => const AnalysisOptionsPage(),
            settings: const RouteSettings(name: "TracesPage")));
  }

  Widget _parameterSlider(
      {required String name,
      required String description,
      required double value,
      double max = 5,
      double min = 0,
      bool discrete = true,
      required void Function(double) onChanged}) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 10),
      child: Column(crossAxisAlignment: CrossAxisAlignment.stretch, children: [
        Text(
          name,
          style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 14),
        ),
        Text(description),
        Slider(
          value: value,
          max: max + 0.0,
          min: 0,
          divisions: discrete ? max.round() * 5 : null,
          label: "$name: ${value.toStringAsFixed(1)}",
          onChanged: onChanged,
        )
      ]),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("analysis options"),
        ),
        body: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: BlocBuilder<AnalysisoptionsCubit, AnalysisoptionsState>(
                builder: (context, state) => state.when(
                    neutral: (options) => Column(
                          children: [
                            _parameterSlider(
                                name: "area size",
                                description:
                                    "how fine the segmentation into areas should be computed. (Impacts performance)",
                                value: options.gridSize,
                                discrete: false,
                                min: 0.0001,
                                max: 0.01,
                                onChanged: (value) => context
                                    .read<AnalysisoptionsCubit>()
                                    .changeOption(
                                        options.copyWith(gridSize: value))),
                            _parameterSlider(
                                name: "distance penalty",
                                description:
                                    "how much areas that are far from a recorded location should be penalized",
                                value: options.distPenalty,
                                max: 5,
                                onChanged: (value) => context
                                    .read<AnalysisoptionsCubit>()
                                    .changeOption(
                                        options.copyWith(distPenalty: value))),
                            _parameterSlider(
                                name: "temporal penalty",
                                description:
                                    "how much areas that were last visited a long time ago should be penalized",
                                value: options.timePenalty,
                                max: 5,
                                onChanged: (value) => context
                                    .read<AnalysisoptionsCubit>()
                                    .changeOption(
                                        options.copyWith(timePenalty: value))),
                            _parameterSlider(
                                name: "sensitive area penalty",
                                description:
                                    "how much areas that were are concidered 'sensitive' should be penalized",
                                value: options.sensitiveAreaPenalty,
                                max: 5,
                                onChanged: (value) => context
                                    .read<AnalysisoptionsCubit>()
                                    .changeOption(options.copyWith(
                                        sensitiveAreaPenalty: value))),
                          ],
                        )))));
  }
}
