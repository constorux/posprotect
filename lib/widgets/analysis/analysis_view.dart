import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sem_posprotect/cubits/analysis_cubit/analysis_cubit.dart';
import 'package:sem_posprotect/cubits/map_cubit/map_cubit.dart';
import 'package:sem_posprotect/services/local_data_service.dart';
import 'package:sem_posprotect/widgets/analysis/analysis_actions_view.dart';

import '../map/map_view.dart';

class AnalysisView extends StatelessWidget {
  const AnalysisView({Key? key}) : super(key: key);

  Widget _analysisSection(BuildContext context, List<PosTrace> traces) {
    return Expanded(
      child: Column(
        children: [
          _mapSection(traces),
          AnalysisActionsView(traces: traces),
        ],
      ),
    );
  }

  Widget _mapSection(List<PosTrace> traces) {
    return Expanded(
        //maxHeight: 400,
        child: BlocBuilder<AnalysisCubit, AnalysisState>(
            builder: (context, state) => state.when(
                initial: () => MapView(traces: traces, gridSize: 1),
                analyzing: () => const Center(
                      child: CircularProgressIndicator(),
                    ),
                error: (msg) => Center(
                      child: Text(msg),
                    ),
                completed: (result) => MapView(
                      traces: traces,
                      analysisSquares: result.areas,
                      sensitiveAreas: result.sensitiveAreas,
                      selectedPosition: result.selectedLocation,
                      gridSize: result.gridSize,
                    ))));
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<MapCubit, MapState>(
        builder: (context, state) => state.when(
            error: (error) => Text(error),
            loading: () => const Center(child: CircularProgressIndicator()),
            neutral: (traces) => _analysisSection(context, traces)));
  }
}
