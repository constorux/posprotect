import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sem_posprotect/cubits/analysis_cubit/analysis_cubit.dart';
import 'package:sem_posprotect/cubits/analysis_options_cubit/analysisoptions_cubit.dart';
import 'package:sem_posprotect/services/local_data_service.dart';
import 'package:sem_posprotect/widgets/analysis/analysis_options_page.dart';

class AnalysisActionsView extends StatelessWidget {
  final List<PosTrace> traces;
  const AnalysisActionsView({Key? key, required this.traces}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.only(top: 30, bottom: 20),
        child: BlocBuilder<AnalysisoptionsCubit, AnalysisoptionsState>(
            builder: (context, state) => state.when(
                neutral: (options) => BlocBuilder<AnalysisCubit, AnalysisState>(
                    builder: (context, state) => state.when(
                        analyzing: () => const Center(
                              child: CircularProgressIndicator(),
                            ),
                        error: (msg) => Center(
                              child: Text(msg),
                            ),
                        initial: () => Row(
                              children: [
                                IconButton(
                                    onPressed: () =>
                                        AnalysisOptionsPage.navigate(context),
                                    icon: const Icon(Icons.settings_outlined)),
                                Expanded(
                                  child: ElevatedButton(
                                      onPressed: () => context
                                          .read<AnalysisCubit>()
                                          .analyse(traces, options),
                                      child: const Text("generate position")),
                                ),
                              ],
                            ),
                        completed: (_) => TextButton(
                            onPressed: () =>
                                context.read<AnalysisCubit>().reset(),
                            child: const Text("reset")))))));
  }
}
