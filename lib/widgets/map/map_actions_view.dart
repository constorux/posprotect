import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sem_posprotect/cubits/analysis_cubit/analysis_cubit.dart';
import 'package:sem_posprotect/cubits/map_options_cubit/map_options_cubit.dart';

class MapActionsView extends StatelessWidget {
  const MapActionsView({Key? key}) : super(key: key);

  Widget _toggle(
      {required IconData icon,
      required bool value,
      required Color activeColor,
      bool enabled = true,
      required void Function(bool) onPressed,
      String? tooltip}) {
    return IconButton(
        onPressed: enabled ? () => onPressed.call(!value) : null,
        icon: Icon(icon),
        color: value ? activeColor : Colors.black,
        tooltip: tooltip);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      //color: Colors.grey.shade100,
      padding: EdgeInsets.all(10),
      child: BlocBuilder<AnalysisCubit, AnalysisState>(
          builder: (_, analysisState) {
        bool analysed = analysisState.maybeWhen(
            orElse: () => false, completed: (_) => true);
        return BlocBuilder<MapOptionsCubit, MapOptionsState>(
          builder: (context, state) => state.map(
              initial: (state) => Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      _toggle(
                          icon: Icons.timeline_rounded,
                          value: state.showTraces,
                          tooltip: "show traces",
                          activeColor: Colors.blue,
                          onPressed: (v) => context
                              .read<MapOptionsCubit>()
                              .setOption(state.copyWith(showTraces: v))),
                      _toggle(
                          icon: Icons.grid_on_rounded,
                          value: state.showAreas,
                          enabled: analysed,
                          tooltip: "show areas",
                          activeColor: Colors.purple,
                          onPressed: (v) => context
                              .read<MapOptionsCubit>()
                              .setOption(state.copyWith(showAreas: v))),
                      _toggle(
                          icon: Icons.home_rounded,
                          value: state.showSensitiveAreas,
                          enabled: analysed,
                          tooltip: "show sensitive areas",
                          activeColor: Colors.orange,
                          onPressed: (v) => context
                              .read<MapOptionsCubit>()
                              .setOption(
                                  state.copyWith(showSensitiveAreas: v))),
                      _toggle(
                          icon: Icons.where_to_vote_rounded,
                          value: state.showPosition,
                          enabled: analysed,
                          tooltip: "show selected location",
                          activeColor: Colors.green,
                          onPressed: (v) => context
                              .read<MapOptionsCubit>()
                              .setOption(state.copyWith(showPosition: v))),
                    ],
                  )),
        );
      }),
    );
  }
}
