import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:flutter_map/plugin_api.dart';
import 'package:latlong2/latlong.dart';
import 'package:sem_posprotect/cubits/map_options_cubit/map_options_cubit.dart';
import 'package:sem_posprotect/services/analyser_service.dart';
import 'package:sem_posprotect/services/local_data_service.dart';
import 'package:sem_posprotect/widgets/map/map_actions_view.dart';

const MAP_URL_OSM = "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png";
const MAP_URL_WMF = "https://tiles.wmflabs.org/bw-mapnik/{z}/{x}/{y}.png";
const MAP_URL_TON = "http://a.tile.stamen.com/toner/{z}/{x}/{y}.png";
const MAP_URL_WAC = "http://c.tile.stamen.com/watercolor/{z}/{x}/{y}.jpg";
const MAP_URL_TER = "http://c.tile.stamen.com/terrain/{z}/{x}/{y}.jpg";
const mapURL = MAP_URL_TON;

class MapView extends StatelessWidget {
  //final LatLng? deviceLocation;
  final List<PosTrace> traces;
  final List<AnalysisPoint> analysisSquares;
  final List<LatLng> sensitiveAreas;
  final LatLng? selectedPosition;
  final double gridSize;
  const MapView(
      {Key? key,
      //required this.deviceLocation,
      required this.traces,
      this.analysisSquares = const [],
      this.sensitiveAreas = const [],
      this.selectedPosition,
      required this.gridSize})
      : super(key: key);

  Marker mapMarker({required LatLng pos, Color color = Colors.blue}) {
    const double size = 10;
    return Marker(
        width: size,
        height: size,
        point: pos,
        builder: (ctx) => Container(
            decoration: BoxDecoration(
                color: color, borderRadius: BorderRadius.circular(20))));
  }

  Widget mapAttribute() {
    return Container(
        padding: const EdgeInsets.all(6),
        decoration: BoxDecoration(
            color: Colors.white.withAlpha(160),
            borderRadius:
                const BorderRadius.only(topLeft: Radius.circular(10))),
        child: const Text("© OpenStreetMap & stamen.com",
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 10,
            )));
  }

  Polygon? overlaySquare(LatLng position,
      {required double size, double opacity = 1, Color color = Colors.purple}) {
    if (opacity < 0.05) return null;
    const fact = 1;
    final double lat = (position.latitude / size).floor() * size;
    final double long =
        (position.longitude / size / fact).floor() * size * fact;
    return Polygon(color: color.withAlpha((255 * opacity).floor()), points: [
      LatLng(lat, long),
      LatLng(lat, long + size * fact),
      LatLng(lat + size, long + size * fact),
      LatLng(lat + size, long),
    ]);
  }

  CircleMarker _sensitiveArea(LatLng position) {
    return CircleMarker(
        point: position, radius: 14, color: Colors.orange.withAlpha(200));
  }

  Marker _positionMarker(LatLng position,
      {double size = 35,
      IconData icon = Icons.room_rounded,
      Color color = Colors.red}) {
    return Marker(
        width: size,
        height: size * 2,
        point: position,
        builder: (ctx) => Align(
              alignment: Alignment.topCenter,
              child: Icon(
                icon,
                color: color,
                size: size,
              ),
            ));
  }

  Widget _map() {
    return BlocBuilder<MapOptionsCubit, MapOptionsState>(
        builder: (context, state) => state.map(
            initial: (state) => FlutterMap(
                  options: MapOptions(
                    interactiveFlags:
                        InteractiveFlag.all & ~InteractiveFlag.rotate,
                    center: traces.isEmpty ? null : traces.last.position,
                    zoom: 13.0,
                  ),
                  layers: [
                    TileLayerOptions(
                      urlTemplate: mapURL,
                      subdomains: ['a', 'b', 'c'],
                      attributionBuilder: (_) => mapAttribute(),
                    ),
                    if (state.showTraces)
                      MarkerLayerOptions(
                        markers: [
                          for (PosTrace trace in traces)
                            mapMarker(pos: trace.position)
                        ],
                      ),
                    if (state.showAreas)
                      PolygonLayerOptions(
                          polygons: analysisSquares
                              .map((e) => overlaySquare(e.anchor,
                                  size: gridSize, opacity: e.score))
                              .whereType<Polygon>()
                              .toList()),
                    if (state.showSensitiveAreas)
                      CircleLayerOptions(
                          circles: sensitiveAreas
                              .map((e) => _sensitiveArea(e))
                              .toList()),
                    if (state.showPosition && selectedPosition != null)
                      MarkerLayerOptions(markers: [
                        _positionMarker(selectedPosition!,
                            icon: Icons.where_to_vote_rounded,
                            color: Colors.green)
                      ]),
                    if (traces.isNotEmpty)
                      MarkerLayerOptions(
                          markers: [_positionMarker(traces.last.position)])
                  ],
                )));
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        clipBehavior: Clip.hardEdge,
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(14),
            boxShadow: const [
              BoxShadow(
                  color: Color(0x35000000),
                  offset: Offset(0, 5),
                  spreadRadius: 0,
                  blurRadius: 16)
            ]),
        child: Column(
          children: [
            Expanded(
              child: _map(),
            ),
            MapActionsView(),
          ],
        ));
  }
}
