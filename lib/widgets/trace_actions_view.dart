import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sem_posprotect/cubits/map_cubit/map_cubit.dart';
import 'package:sem_posprotect/util/tools.dart';
import 'package:sem_posprotect/widgets/traces_page.dart';

class TraceActionsView extends StatelessWidget {
  const TraceActionsView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 10, bottom: 20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          IconButton(
              icon: Icon(Icons.refresh),
              onPressed: () async {
                var l = await context.read<MapCubit>().refresh();
                Tools.showSnackbar(context, "reloaded $l traces");
              }),
          IconButton(
              icon: Icon(Icons.list),
              onPressed: () => TracesPage.navigate(context)),
          IconButton(
              icon: Icon(Icons.delete),
              onPressed: () async {
                await context.read<MapCubit>().deleteTraces();
                Tools.showSnackbar(context, "deleted all traces");
              }),
        ],
      ),
    );
  }
}
