import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sem_posprotect/cubits/map_cubit/map_cubit.dart';
import 'package:sem_posprotect/widgets/analysis/analysis_view.dart';
import 'package:sem_posprotect/widgets/map/map_view.dart';
import 'package:sem_posprotect/widgets/trace_actions_view.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  Widget demoList() {
    return BlocBuilder<MapCubit, MapState>(
        builder: (context, state) => state.when(
            error: (msg) => Text(msg),
            loading: () => const CircularProgressIndicator(),
            neutral: (traces) => Expanded(
                child: ListView.builder(
                    itemCount: traces.length,
                    itemBuilder: (context, i) => Text(traces[i].encode())))));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "posProtect",
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 20),
        child: Column(children: const [TraceActionsView(), AnalysisView()]),
      ),
    );
  }
}
